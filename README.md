## URL

### www.folding-list.trishalim.com

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in development mode.

### `npm run build`

Builds the app for production to the `build` folder

## Task

Build a UI that can fold and unfold a list based on the following:

1. Verticals
2. Categories
3. Courses

Categories have a parent called vertical and courses have a parent called categories.

Use data found in json files in src/data.

## Solution

1. From the json files provided in src/data, create an object structured like a tree (found in buildCourseTree() in src/components/App/App.js.):

```
[
	Id: 1,
	Name: First Item
	Children: [
		Id: 11,
		Name: First Child of First Item,
		Children: [
			Id: 111,
			Name: First Granchild
		]
	],
	Id: 2,
	Name: Second Item,
	Children: [
		Id: 21,
		Name: First Child of Second Item
	]
]
```


2.  Create reusable components (found in src/components).

	FoldingList:
	
	1. Displays list of items
	
	FoldingListItem:
	
	1. Displays a list item
	2. Displays a list under that list item

	

3. Display Verticals, Categories, Courses as a tree using FoldingList and FoldingListItem components.

	Refer to: src/components/Content/Content.js
