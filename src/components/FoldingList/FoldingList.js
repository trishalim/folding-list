import React, { Component } from 'react';

import FoldingListItem from '../FoldingListItem/FoldingListItem';

import './FoldingList.scss';

class FoldingList extends Component {

  render() {
    return (
      <ul className="folding-list-component">

        {
          this.props.list ?
          this.props.list.map(item =>
            <FoldingListItem
              key={item.Id}
              item={item}
            ></FoldingListItem>
          ) : ''
        }

      </ul>
    );
  }

}

export default FoldingList;
