import React, { Component } from 'react';

// Components
import Header from '../Header/Header';
import Content from '../Content/Content';

// CSS style
import './App.scss';

class App extends Component {

  render() {
    return (
      <div className="app-component">
        <Header></Header>
        <Content></Content>
      </div>
    );
  }
}

export default App;
