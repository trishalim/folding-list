import React, { Component } from 'react';

// Components
import FoldingList from '../FoldingList/FoldingList';

// Dummy data
import Verticals from '../../data/verticals.json';
import Categories from '../../data/categories.json';
import Courses from '../../data/courses.json';

class CourseList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      courseTree: null,
    };
  }

  componentDidMount() {
    this.buildCourseTree();
  }

  // Create a tree-like object from Courses, Categories, Verticals
  // This new structure will be passed into FoldingList
  buildCourseTree() {
    const courseMap = new Map();
    Courses.forEach(course => {
      if (courseMap.has(course.Categories)) {
        courseMap.get(course.Categories).push(course);
      } else {
        courseMap.set(course.Categories, [course]);
      }
    });

    const categoryMap = new Map();
    Categories.forEach(category => {
      const verticalId = category.Verticals;
      category.Children = courseMap.get(category.Id);
      if (categoryMap.has(verticalId)) {
        categoryMap.get(verticalId).push(category);
      } else {
        categoryMap.set(verticalId, [category]);
      }
    });

    Verticals.map(vertical =>
      vertical.Children = categoryMap.get(vertical.Id)
    );

    this.setState({
      courseTree: Verticals
    });
  }

  render() {
    return (
      <FoldingList list={this.state.courseTree}></FoldingList>
    );
  }
}

export default CourseList;
