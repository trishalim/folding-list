import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';

// Components
import CourseList from '../CourseList/CourseList';

import './Content.scss';

class Content extends Component {

  render() {
    return (
      <Paper className="content-component">
        <CourseList></CourseList>
      </Paper>
    );
  }
}

export default Content;
