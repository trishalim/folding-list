import React, { Component } from 'react';

// Material UI
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import FoldingList from '../FoldingList/FoldingList';

import './FoldingListItem.scss';

class FoldingListItem extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false
    }
  }

  // fold or unfold list
  toggleList() {
    this.setState({ open: !this.state.open });
  }

  getIcon() {
    if (this.props.item.Children) {
      if (this.state.open) {
        return <ExpandLess />;
      } else {
        return <ExpandMore />;
      }
    }

    return '';
  }

  render() {
    return (
      <li className="folding-list-item-component">
        <div
          className="list-item"
          onClick={this.toggleList.bind(this)}>
          {this.props.item.Name}
          {this.getIcon()}
        </div>
        {
          this.state.open && this.props.item.Children ?
          <FoldingList
            list={this.props.item.Children}
          ></FoldingList>
          : ''
        }
      </li>
    );
  }
}

export default FoldingListItem;
